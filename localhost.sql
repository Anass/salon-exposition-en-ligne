-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2017 at 10:56 ص
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pfe`
--
CREATE DATABASE IF NOT EXISTS `pfe` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pfe`;

-- --------------------------------------------------------

--
-- Table structure for table `achat`
--

CREATE TABLE `achat` (
  `id_achat` int(11) NOT NULL,
  `Id_Client` int(11) NOT NULL,
  `nomberProduit` int(11) NOT NULL,
  `Quantite` int(11) DEFAULT NULL,
  `Montant` float NOT NULL,
  `Date_achat` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `achat`
--

INSERT INTO `achat` (`id_achat`, `Id_Client`, `nomberProduit`, `Quantite`, `Montant`, `Date_achat`) VALUES
(13, 28, 2, NULL, 7500, '0000-00-00'),
(12, 28, 4, NULL, 3424890, '0000-00-00'),
(11, 29, 6, NULL, 1418500, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `automobile`
--

CREATE TABLE `automobile` (
  `Id_automobile` int(50) NOT NULL,
  `Id_Fournisseur` int(11) NOT NULL,
  `Nom_automobile` varchar(50) NOT NULL,
  `Type_Auto` varchar(50) NOT NULL,
  `Prix` int(11) NOT NULL,
  `Nb_visite` int(11) NOT NULL,
  `rate` float NOT NULL,
  `Annee_Modele` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `automobile`
--

INSERT INTO `automobile` (`Id_automobile`, `Id_Fournisseur`, `Nom_automobile`, `Type_Auto`, `Prix`, `Nb_visite`, `rate`, `Annee_Modele`) VALUES
(4, 12, 'Hardtop', 'sport', 306000, 32, 5.1, 2016),
(5, 13, 'Boxster', 'sport', 821000, 3, 3.2, 2016),
(6, 15, 'BRZ', 'sport', 276900, 10, 2.1, 2016),
(7, 18, 'FR-S', 'sport', 295100, 2, 5.5, 2016),
(8, 17, 'NSX', 'sport', 156000, 15, 6.3, 2017),
(9, 19, '540', '', 587500, 50, 5.7, 2017),
(10, 21, 'Colorado', '', 354500, 45, 6.9, 2016),
(11, 22, ' E-Class', '', 687000, 4, 4.9, 2016),
(3, 1, 'GS F', 'sport', 844400, 55, 4.1, 2016),
(2, 5, 'S90', 'sport', 554500, 45, 4.5, 2017),
(1, 4, 'S3', '', 425000, 17, 6.1, 2016),
(12, 23, 'Flex', '', 427100, 12, 4, 2016),
(13, 24, 'Express 3500', '', 500950, 11, 2, 2016),
(14, 25, 'X3', '', 468000, 10, 4.5, 2016),
(15, 26, 'Fusion Hybrid', '', 314300, 27, 6.5, 2016),
(16, 27, 'ES 300h', '', 410200, 23, 7, 2016),
(17, 10, 'Fit', '', 201650, 40, 2.1, 2016),
(18, 12, 'Avalon Hybrid', '', 419500, 34, 3.4, 2016),
(19, 7, 'Mazda6', '', 301, 42, 5.4, 2016);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `Id_Client` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomComplet` varchar(50) NOT NULL,
  `Adresse` varchar(100) NOT NULL,
  `Ville` varchar(20) NOT NULL,
  `Tel` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`Id_Client`, `email`, `password`, `nomComplet`, `Adresse`, `Ville`, `Tel`) VALUES
(29, 'badrakkar@gmail.com', 'BADR01230', 'GACK GO BLACK', '729 HAY MOUJNIBA BLOC B', 'FLORANCE', '672885873'),
(28, 'root@root.com', 'BADR01230', 'badr akkar', 'TURCKISH', 'FLORANCE', '672885873');

-- --------------------------------------------------------

--
-- Table structure for table `compte`
--

CREATE TABLE `compte` (
  `Id_Compte` int(100) NOT NULL,
  `Num_Carte` varchar(100) NOT NULL,
  `Date_experation` date NOT NULL,
  `Code_Secret` varchar(11) NOT NULL,
  `Methode_Paiment` varchar(50) NOT NULL,
  `Id_Client` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `compte`
--

INSERT INTO `compte` (`Id_Compte`, `Num_Carte`, `Date_experation`, `Code_Secret`, `Methode_Paiment`, `Id_Client`) VALUES
(33, '12334532', '0000-00-00', '(345', '', 28),
(32, '12333', '0000-00-00', '''324', '', 28),
(31, '1233454566', '0000-00-00', '"Ã©34432', '', 28),
(30, 'ezf', '0000-00-00', 'fzeezf', '', 28),
(29, '122333', '0000-00-00', 'ezf', '', 28),
(28, '1234566', '0000-00-00', '12324', '', 29),
(27, '1112324324', '0000-00-00', '4324324', '', 28);

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `Id_Fournisseur` int(11) NOT NULL,
  `NomFournisseur` varchar(50) NOT NULL,
  `Adresse` varchar(100) NOT NULL,
  `Ville` varchar(50) DEFAULT NULL,
  `Telephone` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`Id_Fournisseur`, `NomFournisseur`, `Adresse`, `Ville`, `Telephone`) VALUES
(1, ' FANUC', 'ZAC des Folies               \r\n15, rue Léonard de Vinci\r\n91090 Lisses\r\nFRANCE    ', 'Lisses', '+33 1 6989 7000 '),
(5, '3D Robotics', '1608 4th Street\r\nSuite 410\r\nBerkeley, CA 94710 United States', 'Berkeley', '+858 225 1414'),
(2, 'ABB Robotics', 'Affolternstrasse 44\r\nPO Box 8131\r\nZurich, 8050 Switzerland', 'Zurich', '+41 4 3317 7111'),
(3, 'Adele Robots', 'Parque Tecnológico de Asturias\r\nLlanera, Principado de Asturias 22428 Spain', 'Asturias', '+902 286 3866'),
(4, 'AeroVironment', '181 West Huntington Dr.\r\nSuite 202\r\nMonrovia, CA 91016 United States', 'California', '+626 357 9983'),
(7, 'Aesynt', 'Via Altmann, 9/A\r\nBolzano, BZ 39100 Italy', 'Bolzano', '+800 594 9145'),
(8, 'Aethon Inc', '100 Business Center Drive\r\nPittsburgh, PA 15205 United States', 'Pennsylvania', '+412 322 2975'),
(9, 'AMP Robotics', '17795 W 59th Dr\r\nGolden, Colorado 80403 United States', 'Colorado', '+720 470 0812'),
(11, 'Airware', '1045 Bryant St.\r\nSuite 300\r\nSan Francisco, California 94110 United States', 'California', '+877 714 4828'),
(12, 'Aldebaran', '43 rue du Colonel Pierre Avia\r\nParis, 75015 France', 'Paris', '+33 177 371 752'),
(13, 'AllMotion Inc', '30097 Ahern Ave\r\nUnion City, CA 94587 USA', 'California', '+510 471 4000'),
(14, 'Apptronik Systems Inc', '10705 Metric Blvd STE 103 \r\nAustin, TX 78758 USA', 'Texas', '+512 917 8003'),
(15, 'Argo Robotics', '66 Hincks Street\r\nNew Hamburg, Ontario N3A 2A3 Canada', 'Ontario', '+519 662 4229'),
(16, 'Auris Surgical Robotics Inc', '125 Shoreway Road\r\nSuite D\r\nSan Carlos, CA 94070 U.S.A', 'California', '+650 610 0750'),
(17, 'Astrobotic Technology', '4551 Forbes Ave\r\nSuite 300\r\nPittsburgh, PA 15213 United States', 'Pennsylvania', '+412 682 3282'),
(18, 'Autonomous Solutions, Inc', '990 North 8000 West\r\nMendon, Utah 84325 United States', 'Utah ', '+866 881 2171'),
(19, 'Autonomous Marine Systems', '28 Dane St.\r\nSomerville, Massachusetts 2143 United States', 'Massachusetts', '+512 820 2158'),
(20, 'Auro Robotics', '1257 Moonlight Way\r\nMilpitas, California 95035 United States', 'California', '+302 753 4231'),
(21, 'Bharati Robotic Systems', 'S No 165/9, Road No B8/B, Khese Colony\r\n, Pune 411047 India', 'Pune', '+91 9960 412 666'),
(22, 'Barrett Medical', '73 Chapel St.\r\nSuite D\r\nNewton, Massachusetts 2127 United States', 'Massachusetts', '+617 252 9000'),
(23, 'Barrett Technology', '625 Mt Auburn St.\r\nCambridge, MA 2138 United States', 'Massachusetts', '+617 252 9000'),
(24, 'Blue Frog Robotics', '13 rue du Mail\r\nParis, 75002 France', 'Paris', '+118 170 9940'),
(25, 'Blue Belt Technologies Inc.', '2905 Northwest Blvd.\r\nSuite 40\r\nPlymouth, MN 55441 U.S.A', 'Minnesota', '+763 452 4950'),
(26, 'Bosch Group', 'Postfach 30 02 20\r\n70442\r\nStuttgart, Germany', 'Stuttgart', '+711 400 4099'),
(27, 'Genesis Systems Group, LLC', '8900 N. Harrison Street\r\nDavenport, IA 52806 US', 'Iowa', '+563 445 5600'),
(28, 'Harvest Automation Inc', '85 Rangeway Road\r\nBuilding 1 -- First Floor\r\nBillerica, MA 01862 United States', 'Massachusetts', '+978 528 4250'),
(29, 'Future Robot', '901, Uspace 1B, 670\r\nSampyeong-dong, Bundang-gu South Korea', 'Sampyeong-dong', '+031 252 6860'),
(30, 'Clearpath Robotics', '1425 Strasburg Rd.\r\nSuite 2A\r\nKitchener, ON N2R1H2 Canada', 'Ontario', '+519 513 2416'),
(31, 'Caterpillar, Inc.', '501 Southwest Jefferson Avenue\r\nPeoria, IL 61630 United States', 'Illinois', '+888 614 4328'),
(32, 'Catalia Health', '629A Bryant\r\nSan Francisco, CA 94107 USA', 'California', '+415 697 1299'),
(33, 'Neato Robotics', '8100 Jarvis Avenue\r\nNewark, CA 94560 United States', 'California', '+408 848 1063'),
(34, 'Naïo Technologies', 'Villa EL PASO\r\n12 avenue de l’Europe\r\nRamonville, Saint Agne 31520 France', 'Toulouse', '+33 09 7245 4085'),
(35, 'Myrmex, Inc', '2479 E. Bayshore Road, Suite 235\r\nPalo Alto, CA 94303 USA ', 'California', '+650 600 1474'),
(36, 'Lockheed Martin', '6801 Rockledge Drive\r\nBethesda, MD 20817 United States', 'Maryland', '+301 897 6000');

-- --------------------------------------------------------

--
-- Table structure for table `pc`
--

CREATE TABLE `pc` (
  `Id_pc` int(11) NOT NULL,
  `Nom_pc` varchar(100) NOT NULL,
  `Id_Fournisseur` int(11) NOT NULL,
  `type_ordinateur` varchar(50) NOT NULL,
  `Details_techniques` varchar(200) NOT NULL,
  `fonctionnalités speciales` varchar(150) NOT NULL,
  `rate` float NOT NULL,
  `Nb_visite` int(11) NOT NULL,
  `Prix` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pc`
--

INSERT INTO `pc` (`Id_pc`, `Nom_pc`, `Id_Fournisseur`, `type_ordinateur`, `Details_techniques`, `fonctionnalités speciales`, `rate`, `Nb_visite`, `Prix`) VALUES
(1, 'Asus GL702VM', 11, 'portable', '7th Gen Intel® Core™ i7 processor; 13.3" display; 16GB memory; 512GB solid state drive', 'touch screen', 3.45, 27, 1199990),
(2, 'Dell', 9, 'portable', ' 7th generation Intel® Core™ i7 processor; 13.3" display; 16 GB of memory; 256 GB solid state drive', 'fingerprint reader; Bluetooth; touch screen; Thunderbolt 3; USB C 3.1', 4.4, 3, 1299990),
(3, 'HP', 6, 'portable', 'Intel® Pentium™ processor; 11.6" display; 4GB memory; 128GB solid state drive', 'Bluetooth; touch screen; HDMI output', 5.4, 15, 349990),
(4, 'Lenovo Yoga', 5, 'portable', '6th Gen Intel® Core™ i7 processor; 17.3" display; 12GB memory; 1TB hard drive; 128GB solid state drive', 'Bluetooth; G-SYNC; backlit keyboard; HDMI output', 4.5, 35, 1449909);

-- --------------------------------------------------------

--
-- Table structure for table `robot`
--

CREATE TABLE `robot` (
  `Id_robot` int(11) NOT NULL,
  `Id_fournisseur` int(11) NOT NULL,
  `Nom_robot` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `Prix` varchar(100) NOT NULL,
  `Poid` varchar(50) NOT NULL,
  `Nb_visite` int(11) NOT NULL,
  `rate` float NOT NULL,
  `Annee_Fabrication` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `robot`
--

INSERT INTO `robot` (`Id_robot`, `Id_fournisseur`, `Nom_robot`, `type`, `Prix`, `Poid`, `Nb_visite`, `rate`, `Annee_Fabrication`) VALUES
(1, 11, 'ArcMate 0iA', 'indus', '5000', '110kg', 41, 5.1, 2005),
(4, 12, 'IRB 540', 'medical', '2500', '240kg', 8, 4.3, 2009),
(5, 20, 'YuMi', 'medical', '8000', '38kg', 11, 7, 2010),
(6, 1, 'Tico', 'medical', '85000', '85kg', 1, 7.2, 2007),
(7, 2, 'RQ-11 Raven', 'arme', '300000', '7.7 kg', 65, 5.5, 2004),
(8, 3, 'Solo Drone', 'arme', '9999', '1.8kg', 35, 3.2, 2012),
(10, 4, 'Tug', 'sucerity', '100000', '20kg', 21, 6.04, 2014),
(11, 5, 'Ampbot', 'loisir', '200', '5kg', 12, 3.75, 2016);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achat`
--
ALTER TABLE `achat`
  ADD PRIMARY KEY (`id_achat`),
  ADD KEY `Id_Client` (`Id_Client`);

--
-- Indexes for table `automobile`
--
ALTER TABLE `automobile`
  ADD PRIMARY KEY (`Id_automobile`),
  ADD KEY `Id_Fournisseur` (`Id_Fournisseur`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`Id_Client`);

--
-- Indexes for table `compte`
--
ALTER TABLE `compte`
  ADD PRIMARY KEY (`Id_Compte`);

--
-- Indexes for table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`Id_Fournisseur`);

--
-- Indexes for table `pc`
--
ALTER TABLE `pc`
  ADD PRIMARY KEY (`Id_pc`);

--
-- Indexes for table `robot`
--
ALTER TABLE `robot`
  ADD PRIMARY KEY (`Id_robot`),
  ADD KEY `fournisseur` (`Id_fournisseur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `achat`
--
ALTER TABLE `achat`
  MODIFY `id_achat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `automobile`
--
ALTER TABLE `automobile`
  MODIFY `Id_automobile` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `Id_Client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `compte`
--
ALTER TABLE `compte`
  MODIFY `Id_Compte` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `fournisseur`
--
ALTER TABLE `fournisseur`
  MODIFY `Id_Fournisseur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `pc`
--
ALTER TABLE `pc`
  MODIFY `Id_pc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `robot`
--
ALTER TABLE `robot`
  MODIFY `Id_robot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
