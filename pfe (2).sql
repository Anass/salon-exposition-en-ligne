-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 20 Mai 2017 à 13:12
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `pfe`
--

-- --------------------------------------------------------

--
-- Structure de la table `achat`
--

CREATE TABLE IF NOT EXISTS `achat` (
  `id_achat` int(11) NOT NULL,
  `Id_Client` int(11) NOT NULL,
  `nomberProduit` int(11) NOT NULL,
  `Quantite` int(11) DEFAULT NULL,
  `Montant` float NOT NULL,
  `Date_achat` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `achat`
--

INSERT INTO `achat` (`id_achat`, `Id_Client`, `nomberProduit`, `Quantite`, `Montant`, `Date_achat`) VALUES
(0, 7, 3, NULL, 14600, '0000-00-00'),
(0, 7, 2, NULL, 13000, '0000-00-00'),
(0, 7, 1, NULL, 5000, '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `automobile`
--

CREATE TABLE IF NOT EXISTS `automobile` (
  `Id_Auto` int(50) NOT NULL AUTO_INCREMENT,
  `Id_Fournisseur` int(11) NOT NULL,
  `Nom_automobile` int(11) NOT NULL,
  `Type_Auto` varchar(50) NOT NULL,
  `Prix` varchar(100) NOT NULL,
  `Annee_Modele` int(10) NOT NULL,
  PRIMARY KEY (`Id_Auto`),
  KEY `Id_Fournisseur` (`Id_Fournisseur`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `automobile`
--

INSERT INTO `automobile` (`Id_automobile`, `Id_Fournisseur`, `Nom_automobile`, `Type_Auto`, `Prix`, `Annee_Modele`) VALUES
(1, 37, 'S3', '', '425 000 dh\r\n', 2016),
(2, 49, 'S90', 'sport', '554500 ', 2017),
(3, 43, 'GS F', 'sport', '844400 ', 2016),
(4, 38, 'Hardtop', 'sport', '306000 ', 2016),
(5, 46, 'Boxster', 'sport', '821000 ', 2016),
(6, 47, 'BRZ', 'sport', '276900 ', 2016),
(7, 48, 'FR-S', 'sport', '295100 ', 2016),
(8, 41, 'NSX', 'sport', '156000 ', 2017),
(9, 38, '540', '', '587500 ', 2017),
(10, 39, 'Colorado', '', '354500', 2016),
(11, 45, ' E-Class', '', '687000 ', 2016),
(12, 40, 'Flex', '', '427100 ', 2016),
(13, 39, 'Express 3500', '', '500950', 2016),
(14, 38, 'X3', '', '468000 ', 2016),
(15, 40, 'Fusion Hybrid', '', '314300 ', 2016),
(16, 43, 'ES 300h', '', '410200 ', 2016),
(17, 42, 'Fit', '', '201650 ', 2016),
(18, 48, 'Avalon Hybrid', '', '419500 ', 2016),
(19, 44, 'Mazda6', '', '301 950 ', 2016);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `Id_Client` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomComplet` varchar(50) NOT NULL,
  `Adresse` varchar(100) NOT NULL,
  `Ville` varchar(20) NOT NULL,
  `Tel` varchar(20) NOT NULL,
  PRIMARY KEY (`Id_Client`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`Id_Client`, `email`, `password`, `nomComplet`, `Adresse`, `Ville`, `Tel`) VALUES
(7, 'rout@rout.com', 'rout', 'rout toor', 'boulvard hay al kodsse', 'oujda', '0652214778');

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE IF NOT EXISTS `compte` (
  `Id_Compte` int(100) NOT NULL AUTO_INCREMENT,
  `Num_Carte` varchar(100) NOT NULL,
  `Date_experation` date NOT NULL,
  `Code_Secret` varchar(11) NOT NULL,
  `Methode_Paiment` varchar(50) NOT NULL,
  `Id_Client` int(11) NOT NULL,
  PRIMARY KEY (`Id_Compte`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `compte`
--

INSERT INTO `compte` (`Id_Compte`, `Num_Carte`, `Date_experation`, `Code_Secret`, `Methode_Paiment`, `Id_Client`) VALUES
(6, '1222457', '0000-00-00', '441125', '', 7);

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

CREATE TABLE IF NOT EXISTS `fournisseur` (
  `Id_Fournisseur` int(11) NOT NULL AUTO_INCREMENT,
  `NomFournisseur` varchar(50) NOT NULL,
  `Adresse` varchar(100) NOT NULL,
  `Ville` varchar(20) NOT NULL,
  `Telephone` varchar(20) NOT NULL,
  `Nb_visite` int(11) NOT NULL,
  PRIMARY KEY (`Id_Fournisseur`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Contenu de la table `fournisseur`
--

INSERT INTO `fournisseur` (`Id_Fournisseur`, `NomFournisseur`, `Adresse`, `Ville`, `Telephone`, `Nb_visite`) VALUES
(1, ' FANUC', 'ZAC des Folies               \r\n15, rue Léonard de Vinci\r\n91090 Lisses\r\nFRANCE    ', 'Lisses', '+33 1 6989 7000 ', 1),
(5, '3D Robotics', '1608 4th Street\r\nSuite 410\r\nBerkeley, CA 94710 United States', 'Berkeley', '+858 225 1414', 1),
(40, 'Ford', 'Dearborn, Michigan, États-Unis', 'Michigan', '+1 800-392-3673', 0),
(41, 'Acura', 'Minato-ku, Tokyo, Japon', 'Tokyo', '+1 800-382-2238', 2),
(2, 'ABB Robotics', 'Affolternstrasse 44\r\nPO Box 8131\r\nZurich, 8050 Switzerland', 'Zurich', '+41 4 3317 7111', 1),
(3, 'Adele Robots', 'Parque Tecnológico de Asturias\r\nLlanera, Principado de Asturias 22428 Spain', 'Asturias', '+902 286 3866', 1),
(4, 'AeroVironment', '181 West Huntington Dr.\r\nSuite 202\r\nMonrovia, CA 91016 United States', 'California', '+626 357 9983', 1),
(7, 'Aesynt', 'Via Altmann, 9/A\r\nBolzano, BZ 39100 Italy', 'Bolzano', '+800 594 9145', 1),
(8, 'Aethon Inc', '100 Business Center Drive\r\nPittsburgh, PA 15205 United States', 'Pennsylvania', '+412 322 2975', 1),
(9, 'AMP Robotics', '17795 W 59th Dr\r\nGolden, Colorado 80403 United States', 'Colorado', '+720 470 0812', 1),
(37, 'Audi', 'Ingolstadt, Allemagne', 'Ingolstadt', '+1 800-822-2834', 0),
(38, 'BMW', ' Munich, Allemagne', ' Munich', '+1 800-831-1117', 0),
(39, 'Chevrolet', 'Détroit, Michigan, États-Unis', 'Michigan', ' +1 800-222-1020 ', 0),
(11, 'Airware', '1045 Bryant St.\r\nSuite 300\r\nSan Francisco, California 94110 United States', 'California', '+877 714 4828', 1),
(12, 'Aldebaran', '43 rue du Colonel Pierre Avia\r\nParis, 75015 France', 'Paris', '+33 177 371 752', 1),
(13, 'AllMotion Inc', '30097 Ahern Ave\r\nUnion City, CA 94587 USA', 'California', '+510 471 4000', 1),
(14, 'Apptronik Systems Inc', '10705 Metric Blvd STE 103 \r\nAustin, TX 78758 USA', 'Texas', '+512 917 8003', 1),
(15, 'Argo Robotics', '66 Hincks Street\r\nNew Hamburg, Ontario N3A 2A3 Canada', 'Ontario', '+519 662 4229', 1),
(16, 'Auris Surgical Robotics Inc', '125 Shoreway Road\r\nSuite D\r\nSan Carlos, CA 94070 U.S.A', 'California', '+650 610 0750', 1),
(17, 'Astrobotic Technology', '4551 Forbes Ave\r\nSuite 300\r\nPittsburgh, PA 15213 United States', 'Pennsylvania', '+412 682 3282', 1),
(18, 'Autonomous Solutions, Inc', '990 North 8000 West\r\nMendon, Utah 84325 United States', 'Utah ', '+866 881 2171', 1),
(19, 'Autonomous Marine Systems', '28 Dane St.\r\nSomerville, Massachusetts 2143 United States', 'Massachusetts', '+512 820 2158', 1),
(20, 'Auro Robotics', '1257 Moonlight Way\r\nMilpitas, California 95035 United States', 'California', '+302 753 4231', 1),
(21, 'Bharati Robotic Systems', 'S No 165/9, Road No B8/B, Khese Colony\r\n, Pune 411047 India', 'Pune', '+91 9960 412 666', 1),
(22, 'Barrett Medical', '73 Chapel St.\r\nSuite D\r\nNewton, Massachusetts 2127 United States', 'Massachusetts', '+617 252 9000', 1),
(23, 'Barrett Technology', '625 Mt Auburn St.\r\nCambridge, MA 2138 United States', 'Massachusetts', '+617 252 9000', 1),
(24, 'Blue Frog Robotics', '13 rue du Mail\r\nParis, 75002 France', 'Paris', '+118 170 9940', 0),
(25, 'Blue Belt Technologies Inc.', '2905 Northwest Blvd.\r\nSuite 40\r\nPlymouth, MN 55441 U.S.A', 'Minnesota', '+763 452 4950', 1),
(26, 'Bosch Group', 'Postfach 30 02 20\r\n70442\r\nStuttgart, Germany', 'Stuttgart', '+711 400 4099', 0),
(27, 'Genesis Systems Group, LLC', '8900 N. Harrison Street\r\nDavenport, IA 52806 US', 'Iowa', '+563 445 5600', 0),
(28, 'Harvest Automation Inc', '85 Rangeway Road\r\nBuilding 1 -- First Floor\r\nBillerica, MA 01862 United States', 'Massachusetts', '+978 528 4250', 1),
(29, 'Future Robot', '901, Uspace 1B, 670\r\nSampyeong-dong, Bundang-gu South Korea', 'Sampyeong-dong', '+031 252 6860', 1),
(30, 'Clearpath Robotics', '1425 Strasburg Rd.\r\nSuite 2A\r\nKitchener, ON N2R1H2 Canada', 'Ontario', '+519 513 2416', 0),
(31, 'Caterpillar, Inc.', '501 Southwest Jefferson Avenue\r\nPeoria, IL 61630 United States', 'Illinois', '+888 614 4328', 1),
(32, 'Catalia Health', '629A Bryant\r\nSan Francisco, CA 94107 USA', 'California', '+415 697 1299', 0),
(33, 'Neato Robotics', '8100 Jarvis Avenue\r\nNewark, CA 94560 United States', 'California', '+408 848 1063', 1),
(34, 'Naïo Technologies', 'Villa EL PASO\r\n12 avenue de l’Europe\r\nRamonville, Saint Agne 31520 France', 'Toulouse', '+33 09 7245 4085', 1),
(35, 'Myrmex, Inc', '2479 E. Bayshore Road, Suite 235\r\nPalo Alto, CA 94303 USA ', 'California', '+650 600 1474', 0),
(36, 'Lockheed Martin', '6801 Rockledge Drive\r\nBethesda, MD 20817 United States', 'Maryland', '+301 897 6000', 0),
(42, 'Honda', 'Minato-ku, Tokyo, Japon', 'Tokyo', '+371 67 819 712', 1),
(43, 'Lexus', 'Nagoya, Préfecture d''Aichi, Japon', 'Nagoya', ' +1 800-255-3987', 2),
(44, 'Mazda', ' Fuchū, Préfecture de Hiroshima, Japon', ' Fuchū', ' +1 800-222-5500', 3),
(45, 'Mercedes-Benz', ' Stuttgart, Allemagne', ' Stuttgart', '+1 800-367-6372', 5),
(46, 'Porsche', 'Stuttgart, Allemagne', 'Stuttgart ', '+1 800-767-7243', 0),
(47, 'Subaru', 'Shibuya, Tokyo, Japon', 'Tokyo', ' +1 800-782-2783', 0),
(48, 'Toyota', 'Toyota, Préfecture d''Aichi, Japon', 'Toyota', '+1 800-331-4331', 0),
(49, 'Volvo ', 'Göteborg, Suède', 'Göteborg', ' +1 800-550-5658', 0),
(50, 'Dell', ' Round Rock, Texas, États-Unis', 'Texas', '+1 800-624-9897', 0),
(51, 'Hewlett-Packard', 'Palo Alto, Californie, États-Unis', 'Californie', '+1 800-474-6836', 0),
(52, 'Asus', 'Taipei, Taïwan', 'Taipei', '+1 888-678-3688', 0),
(53, 'Acer', 'Nouveau Taipei, Taïwan', 'Nouveau Taipei', '+1 866-695-2237', 0),
(54, 'Lenovo', 'Morrisville, Caroline du Nord, États-Unis', 'Caroline du Nord', '09 75 18 17 23', 0);

-- --------------------------------------------------------

--
-- Structure de la table `ordinateur`
--

CREATE TABLE IF NOT EXISTS `pc` (
  `Id_pc` int(11) NOT NULL AUTO_INCREMENT,
  `Modele_Ordinateur` varchar(100) NOT NULL,
  `Id_Fournisseur` int(11) NOT NULL,
  `type_ordinateur` varchar(50) NOT NULL,
  `Détails techniques` varchar(200) NOT NULL,
  `fonctionnalités speciales` varchar(150) NOT NULL,
  `Prix` int(11) NOT NULL,
  PRIMARY KEY (`Id_Ordinateur`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `ordinateur`
--

INSERT INTO `ordinateur` (`Id_pc`, `Modele_Ordinateur`, `Id_Fournisseur`, `type_ordinateur`, `Détails techniques`, `fonctionnalités speciales`, `Prix`) VALUES
(1, 'Spectre x360 13-AC023DX', 51, 'portable', '7th Gen Intel® Core™ i7 processor; 13.3" display; 16GB memory; 512GB solid state drive', 'touch screen', 1199990),
(2, 'XPS XPS9365-7086SLV-PUS', 50, 'portable', ' 7th generation Intel® Core™ i7 processor; 13.3" display; 16 GB of memory; 256 GB solid state drive', 'fingerprint reader; Bluetooth; touch screen; Thunderbolt 3; USB C 3.1', 1299990),
(3, 'Yoga 710 80TX0007US', 54, 'portable', 'Intel® Pentium™ processor; 11.6" display; 4GB memory; 128GB solid state drive', 'Bluetooth; touch screen; HDMI output', 349990),
(4, 'GL702VM-BHI7N09', 52, 'portable', '6th Gen Intel® Core™ i7 processor; 17.3" display; 12GB memory; 1TB hard drive; 128GB solid state drive', 'Bluetooth; G-SYNC; backlit keyboard; HDMI output', 1449909);

-- --------------------------------------------------------

--
-- Structure de la table `robot`
--

CREATE TABLE IF NOT EXISTS `robot` (
  `Id_Robot` int(11) NOT NULL,
  `Id_Fournisseur` int(11) NOT NULL,
  `NomRobot` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `Prix` varchar(100) NOT NULL,
  `Poid` varchar(50) NOT NULL,
  `nbVisite` int(11) NOT NULL,
  `nbachat` int(11) NOT NULL,
  `Annee_Fabrication` int(11) NOT NULL,
  KEY `Id_Fournisseur` (`Id_Fournisseur`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `robot`
--

INSERT INTO `robot` (`Id_Robot`, `Id_Fournisseur`, `NomRobot`, `type`, `Prix`, `Poid`, `nbVisite`, `nbachat`, `Annee_Fabrication`) VALUES
(1, 0, 'ArcMate 0iA', 'indus', '5000', '110kg', 0, 0, 2005),
(2, 0, 'ArcMate 100i', 'indus', '4000', '290kg', 0, 0, 2007),
(3, 0, 'LR Mate 200iB/5C', 'indus', '5600', '45kg', 0, 0, 2008),
(4, 0, 'IRB 540', 'medical', '2500', '240kg', 0, 0, 2009),
(5, 0, 'YuMi', 'medical', '8000', '38kg', 0, 0, 2010),
(6, 0, 'Tico', 'medical', '85000', '85kg', 0, 0, 2007),
(7, 0, 'RQ-11 Raven', 'arme', '300000', '7.7 kg', 0, 0, 2004),
(8, 0, 'Solo Drone', 'arme', '9999', '1.8kg', 0, 0, 2012),
(9, 0, 'PROmanager-Rx™', 'sucerity', '275000000', '100kg', 0, 0, 2013),
(10, 0, 'Tug', 'sucerity', '100000', '20kg', 0, 0, 2014),
(11, 0, 'Ampbot', 'loisir', '200', '5kg', 0, 0, 2016);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
