<?php
session_start();
if(isset($_GET['p'])){
  $page = $_GET['p'];
  if ($page === "home" ){ /* HOME */

    require "../app/controllers/homeController.php";
    $category = new homeController();
    $category->index();

  }else if ( $page=="category" ) { /* CATEGORY */

    $c=$_GET['c']; // $c = robot || automobile ||
    require "../app/controllers/categoryController.php";
    $category = new categoryController();
    $category->index($c);

  }elseif ($page === "panier" ) { /* PANIER*/

    require "../app/views/pages/panier.php";

  }elseif ($page === "buy") { /* buy */

    require "../app/controllers/buyController.php";
    $buy = new buyController();
    if(isset($_GET['idp']) && isset($_GET['category'])){
      //stoket element dans le panier un seul produit
      $buy->buyOne($_GET['idp'],$_GET['category']);
    }
    if(isset($_POST['first'])){
      $_SESSION['errors'] = $buy->firstValidation($_POST);
    }elseif(isset($_POST['client'])) {
      $_SESSION['ver'] = $buy->client($_POST);
    }
    $buy->index();
    unset($_SESSION['errors']);
	  unset($_SESSION['ver']);

  }elseif ($page === "info" ) { /* Info */

    $id=$_GET['in']; // id
    $category = $_GET['category'];
  	require "../app/controllers/infoController.php";
  	$info = new infoController();
  	$info->index($id,$category);

  }elseif ($page === "ajouter"){ /* ADD */

    require "../app/controllers/addController.php";
    $add = new addController();
    $id = $_GET['id'];
    $category = $_GET['category'];
    $add->index($id,$category);

  }elseif ($page === "showmore" ){ /* Show more*/

    require "../app/controllers/showController.php";
    $show = new showController();
    $show->show();

  }elseif ($page  === "delete") { /* DELETE */

    require "../app/controllers/addController.php";
    $produit = $_GET['produit'];
    $delete = new addController();
    $delete->delete($produit);

  }elseif ($page  === "statistical") { /* STATISTIQUE */

    require "../app/controllers/statisticalController.php";
    $stats = new statisticalController();
    if(isset($_GET['about']) && !empty($_GET['about'])){
      //require "../app/views/pages/about.php";
      $stats->index("about");
    }else
    $stats->index("statistique");

  }elseif($page === "notification" ) {
    require "../app/views/notification.php";
  }elseif ($page  === "compte") { /* Compte */

    require "../app/controllers/compteController.php";
  	$compte = new compteController();
    $action = " ";
    if(isset($_GET['action'])){
      $action = $_GET['action'];
      if(isset($_POST[$action])){
        $_SESSION[$action] = $compte->$action($_POST);
      }
    }
    $compte->index();
    unset($_SESSION[$action]);

  }else {
    echo "this page not found";
  }
}else {
  require "../app/views/pages/home.php";
}
?>
