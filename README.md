


Dans le cadre de notre projet de fin d’étude, nous proposons la réalisation d’une application web pour la gestion d’un salon d’exposition en ligne (un site d’exposition virtuel) de différents produits électroniques (Robots, Automobiles, Ordinateurs). Pour faciliter les ventes de ces produits, leurs publicités et créer des liens entre vendeurs et acheteurs même s’ils sont sur des endroits géographiquement éloignés.
 

1. Présentation du projet 

Qu’est-ce qu’un salon d’exposition en ligne?

     Grace à un salon d’exposition en ligne, on peut choisir et payer des produits comme dans un salon d’exposition réel. 
Pour acheter un produit de ce salon virtuel, il suffit de choisir les produits désirés puis de les mettre dans un panier d’achat.
L’acheteur doit ensuite remplir un formulaire d’inscription qui contient les informations personnelles et aussi les informations de la carte bancaire s’il n’est pas déjà inscrit dans le site.

 
2. Objectifs de l’application

L’objectif du projet consiste à développer une application web d’un salon d’exposition de produits électroniques.
Cette application web permettra de réaliser les opérations suivantes :

Gérer les relations avec les clients,
Gérer les relations avec les fournisseurs,
Gérer les produits (ajouter, modifier ou supprimer des produits).

En effet, Notre application web va être comme étant un intermédiaire entre le client et le fournisseur de produit. Elle expose les différents produits des fournisseurs et elle lui permet au client de les acheter en ligne. De plus de ça, elle nous permet aussi de savoir les produits et les fournisseurs les plus demandé afin d’arriver à connaitre mieux les besoins des clients pour prendre des décisions pour le futur afin d’améliorer le site.

3. Technologies et langages utilisés

- Bootstrap
- Ajax
- jQuery
- MySQL
- PHP

