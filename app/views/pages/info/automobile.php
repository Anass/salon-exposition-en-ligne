<div class="col-md-6">
   <a href="#" class="thumbnail">
     <img  src="../ressource/automobile/<?= $data['Nom_automobile'] ; ?>.png" alt="automobile">
   </a>
 </div>

 <div class="col-md-offset-1 col-md-5">

   <a href="#" class="list-group-item active">
      Fiche technique
    </a>

    <ul class="list-group">
     <li class="list-group-item">Model : <?= $data['Nom_automobile'] ;?></li>
     <li class="list-group-item">Type : <?= $data['Type_Auto'] ;?></li>
     <li class="list-group-item">Prix : <?= $data['Prix'] ;?> DH</li>
     <li class="list-group-item">Date fabrication : <?= $data['Annee_Modele'] ;?></li>
    </ul>

    <a href="#" class="list-group-item active">
       Information Fournisseur
     </a>

     <ul class="list-group">

      <li class="list-group-item">fournisseur : <?= $data['NomFournisseur'] ;?></li>
      <li class="list-group-item">Adresse : <?= $data['Adresse'] ;?></li>
      <li class="list-group-item">Ville : <?= $data['Ville'] ;?></li>
      <li class="list-group-item">Telephone : <?= $data['Telephone'] ;?> DH</li>
     </ul>

 </div>
