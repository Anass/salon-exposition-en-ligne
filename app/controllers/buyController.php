<?php
require "../app/models/buyModel.php";
/**
 * a ce neveau on utilise les model
 */
class buyController{

  public $errors = [];

  public function index(){
    require "../app/views/pages/buy.php";
  }

  public function firstValidation($data){
    $buy = new buyModel();
    if (isset($data) && !empty($data)){
      //!filter_var($email, FILTER_VALIDATE_EMAIL)
      if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){
        $this->errors['email'] = "cette email n'est pas valide";
      }
      //$res = $buy->only($data['email']);
      if($buy->only($data['email'])){ // si on trouve des resultats
        $this->errors['email'] =  "ce email est deja existe ";
      }

      if(empty($data['password'])){ // autre traitment
        $this->errors['password'] = "mot de passe invalide";
      }
      if(empty($data['nomcomplet'])){
        $this->errors['nomcomplet'] = "nom complet n'est pas valide";
      }
      if(empty($data['adresse'])){
        $this->errors['adresse'] = "adresse n'est pas valide";
      }
      if(empty($data['ville'])){
        $this->errors['ville'] = "nom de ville n'est pas valide";
      }
      if(empty($data['tele'])){
        $this->errors['nomcomplet'] = "numero tele n'est pas valide";
      }
      if(empty($data['numerocarte'])){
        $this->errors['numerocarte'] = "numero carte n'est pas valide";
      }
      if(empty($data['dateExperation'])){
        $this->errors['dateExperation'] = "date experation n'est pas valide";
      }
      if(empty($data['cvv'])){
        $this->errors['cvv'] = "cvv n'est pas valide";
      }
      if ($_SESSION['prix'] == 0 ) {
        $this->errors['prix'] = "votre panier est vide";
      }
      // verfication
      if(empty($this->errors)){
        // stoker base donnne
        $buy->newClient($data);
    		//$buy->achat();
      }else {
        return $this->errors;
      }

    }
  }

  public function client($data){
	  $buy = new buyModel();
    $res = $buy->only($data['emailClient']);
		if($res){// $res un true
			if( $data['password'] == $res['password']){
        if($_SESSION['prix'] == 0){
          return "vous dever choisir un produit ou remplir votre panier";
        }else{
          $buy->achat($res['Id_Client']);
        }
			}else{
				return "mot de passe incorrrect";
			}
		}else{
			return "Ce email n'exist pas";
		}
  }

  public function buyOne($id,$category){
    $buy = new buyModel();
    $buy->addVisite($id,$category);
    $res = $buy->getProduit($id,$category);
    if($res){
      unset($_SESSION['nb']);
      unset($_SESSION['prix']);
      $_SESSION['nb'] = 1;
      $_SESSION['prix'] = $res['Prix'];
    }else{
      var_dump("produit n'esist pas");
    }

  }


}


?>
