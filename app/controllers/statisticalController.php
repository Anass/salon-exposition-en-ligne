<?php
require "../app/models/statisticalModel.php";
/**
 * a ce neveau on utilise les model
 */
class statisticalController{

  public function index($page){
    $status = new statisticalModel();

    if($page === "about"){
      $robot = $status->robot('robot');
      $auto = $status->robot('automobile');
      $pc = $status->robot('pc');
      require "../app/views/pages/about.php";
    }else{
      $robot_s   =  $status->robot_s('robot');
      $auto_s   =  $status->robot_s('automobile');
      $pc_s   =  $status->robot_s('pc');
      require "../app/views/pages/statistique.php";
    }
  }



}


?>
