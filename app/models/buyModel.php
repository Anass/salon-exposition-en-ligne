<?php
require "../app/models/model.php";
/**
 *
 */
class buyModel extends model{
  private $pdo;

  public function __construct(){
    $this->pdo = $this->pdo(); // $pdo = connextion  base donnee
  }

  public function newClient($data){
    //$req = $this->pdo->prepare("SELECT * from robot WHERE Id_Robot = :Id_Robot ");
    $req = $this->pdo->prepare('INSERT INTO client(email,password ,nomComplet, Adresse, Ville, Tel) VALUES(:email,:password,:nomComplet, :Adresse, :Ville, :Tel)');
    $req->execute(array(
    	'email' => $data['email'] ,
		  'password' => $data['password'] ,
    	'nomComplet' => $data['nomcomplet'] ,
    	'Adresse' => $data['adresse'] ,
    	'Ville' => $data['ville'] ,
    	'Tel' => $data['tele']
    	));
	// select
  	$req2 = $this->pdo->prepare('SELECT Id_Client FROM client WHERE email=:email');
  	$req2->bindValue(":email",$data['email'],PDO::PARAM_STR);
    $req2->execute();
  	$res= $req2->fetch();

    $req = $this->pdo->prepare('INSERT INTO compte(Num_Carte,Date_experation ,Code_Secret,Id_Client) VALUES(:Num_Carte,:Date_experation,:Code_Secret,:Id_Client)');
    $req->execute(array(
    	'Num_Carte' => $data['numerocarte'] ,
		  'Date_experation' => $data['dateExperation'] ,
    	'Code_Secret' => $data['cvv'],
      'Id_Client' => $res['Id_Client']
    	));
    // inserer achat
	$this->achat($res['Id_Client']);
  }

  public function achat($id){
	$req = $this->pdo->prepare('INSERT INTO achat(Id_Client,nomberProduit,Montant) VALUES(:Id_Client,:nomberProduit,:Montant)');
    $req->execute(array(
    	'Id_Client' => $id,
      'nomberProduit' => $_SESSION['nb'],
		  //'Quantite' =>  ,// a recalculer
    	'Montant' => $_SESSION['prix']
    	));
  }


  public function only($email){
	$req = $this->pdo->prepare('SELECT 	Id_Client, email,password FROM client WHERE email=:email ');
    $req->execute(array(
    	'email' => $email
    	));
    return $req->fetch();
  }

  public function getProduit($id,$category){
    $key = 'Id_'.$category;
    $req = $this->pdo->prepare("SELECT $key , Prix FROM $category WHERE $key=:$key");
    $req->execute(array(
        $key => $id
      ));
    return $req->fetch();
  }

  public function addVisite($id,$category){
    $key = 'Id_'.$category;
    $req = $this->pdo->prepare("UPDATE $category SET 	Nb_visite = Nb_visite + 1  WHERE $key=:$key");
    $req->execute(array(
        $key => $id
        ));
  }

}


?>
