(function($){
    // ajouter un produit au panier
    $('.add').on('click',function(e){
    e.preventDefault();
    var $a = $(this);
    var url = $a.attr('href');
    $.ajax(url,{
      success:function(){
        $('#myModal').modal();
      },
      error:function(){
        console.log("non");
      }
    });
  });

  $('.delete').on('click',function(e){
    e.preventDefault();
    var $a = $(this);
    var url = $a.attr('href');
    $.ajax(url,{
      success: function(data,textStatus,jqXHR){
        $a.parents('tr').fadeOut();
        //$('.panier:nth-child(2)').text(data);
        console.log(data);
      },
      error: function(){
        alert("ERROR");
      }
    });
  });

  $('.showmore').on('click',function(e){
    e.preventDefault();
    var $a = $(this);
    var url = $a.attr('href');
    $.ajax(url,{
      success:function(data){
        //console.log(data)
        $('#__row_2').append(data);
        $('.showmore').hide();
      },
      error:function(){
        console.log("non");
      }
    });

  });
})(jQuery);
